package com.example.demo.domain;

import org.junit.Assert;
import org.junit.Test;

public class CourierTest {

    @Test
    public void shouldCallCourierWhenThereIsAvailableCourier(){
        //Arrange
        Courier sut = new Courier();
        Person person = new Person("sibel", "can","05053334466", new Address("il","ilce","sokak","numara"));
        String date = "8 ekim 2020 10:00";
        //Act
        sut.createAppointment(person,date);

        //Assert
        Assert.assertEquals(date,sut.getDate());
    }

}
