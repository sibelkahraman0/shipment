package com.example.demo.domain;

import org.junit.Assert;
import org.junit.Test;
import org.junit.jupiter.api.BeforeEach;

public class ShipmentTest {

    @Test
    public void shouldCreateShipment(){
        //Arrange
        Shipment sut = new Shipment();
        Person sender = new Person("sibel", "kahraman","05053334466", new Address("il","ilce","sokak","numara"));
        Person receiver = new Person("sibel", "can","05053334466", new Address("il","ilce","sokak","numara"));
        ShipmentPackage shipmentPackage = new ShipmentPackage(10,2,4,10);
        //Act
        sut.setSender(sender);
        sut.setReceiver(receiver);
        sut.setShipmentPackage(shipmentPackage);
        sut.setShipmentType("box");
        sut.setPayer("Sender");
        //Assert
        Assert.assertEquals(sender,sut.getSender());
    }

    @Test
    public void shouldUpdateShipment(){
        //Arrange
        Shipment sut = new Shipment();
        Person receiver = new Person("sibel", "can","05053334466", new Address("il","ilce","sokak","numara"));
        sut.setReceiver(receiver);
        Address newAddress = new Address("zonguldak","merkez","cadde","15");

        //Act
        sut.updateShipmentReceiverAddress(newAddress);

        //Assert
        Assert.assertEquals(newAddress, sut.getReceiver().getAddress());
    }

    @Test
    public void shouldChangeShipmentStatus(){
        //Arrange
        Shipment sut = new Shipment();
        String status = "Yolda";
        //Act
        sut.setStatus(status);
        //Assert
        Assert.assertEquals(status,sut.getStatus());
    }

    @Test
    public void shouldCalculatePayment(){
        //Arrange
        Shipment sut = new Shipment();
        ShipmentPackage shipmentPackage = new ShipmentPackage(2,4,6,5);
        //Act
        sut.setShipmentPackage(shipmentPackage);
        //Arrange
        Assert.assertEquals(24,sut.getPayment());

    }



}
