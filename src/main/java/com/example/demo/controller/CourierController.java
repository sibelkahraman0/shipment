package com.example.demo.controller;

import com.example.demo.domain.Courier;
import com.example.demo.domain.Person;
import com.example.demo.exception.courier.CourierNotCreatedException;
import com.example.demo.exception.courier.CourierNotFoundException;
import com.example.demo.service.CourierService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/couriers")
public class CourierController {

    private final CourierService courierService;

    public CourierController(CourierService courierService) {
        this.courierService = courierService;
    }

    @PostMapping()
    public ResponseEntity callCourier(@RequestBody Courier courier) {
        try {
            courierService.createAppointment(courier);
            return ResponseEntity.ok().build();
        } catch (CourierNotCreatedException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getDate(@PathVariable String id) {
        try {
            return ResponseEntity.ok(courierService.getDate(id));
        } catch (CourierNotFoundException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PatchMapping("/{id}")
    public ResponseEntity updateCourier(@PathVariable String id, @RequestBody String date) {
        try {
            courierService.updateCourier(id, date);
            return ResponseEntity.ok().build();
        } catch (CourierNotFoundException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

}
