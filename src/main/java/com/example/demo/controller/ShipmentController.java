package com.example.demo.controller;

import com.example.demo.domain.Address;
import com.example.demo.domain.Shipment;
import com.example.demo.domain.ShipmentPackage;
import com.example.demo.exception.shipment.*;
import com.example.demo.service.ShipmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/shipments")
public class ShipmentController {
    private final ShipmentService shipmentService;

    @Autowired
    public ShipmentController(ShipmentService shipmentService) {
        this.shipmentService = shipmentService;
    }

    @PostMapping()
    public ResponseEntity createShipment(@RequestBody Shipment shipment) {
        try {
            shipmentService.createShipment(shipment);
            URI location = URI.create(String.format("/shipments/%s", shipment.getId()));
            return ResponseEntity.created(location).build();
        } catch (ShipmentNotCreatedException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{trackingCode}")
    public ResponseEntity getShipmentByTrackingCode(@PathVariable String trackingCode) {
        try {
            return ResponseEntity.ok(shipmentService.getShipmentByTrackingCode(trackingCode));
        } catch (ShipmentNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/payments")
    public ResponseEntity calculatePayment(@RequestParam int width, int height, int weight, int length) {
        try {
            return ResponseEntity.ok(shipmentService.calculatePayment(width, height, weight, length));
        } catch (ShipmentNotCalculatedException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PatchMapping("/{trackingCode}")
    public ResponseEntity updateShipment(@PathVariable String trackingCode, @RequestBody Address address) {
        try {
            shipmentService.updateShipment(trackingCode, address);
            URI location = URI.create(String.format("/trackingCode/%s", trackingCode));
            return ResponseEntity.created(location).build();
        } catch (ShipmentNotUpdatedException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PatchMapping("/status/{trackingCode}")
    public ResponseEntity updateStatus(@RequestBody String status, @PathVariable String trackingCode) {
        try {
            shipmentService.updateStatus(trackingCode, status);
            URI location = URI.create(String.format("/trackingCode/%s", trackingCode));
            return ResponseEntity.created(location).build();
        } catch (ShipmentNotStatusUpdatedException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

}
