package com.example.demo.controller;

import com.example.demo.domain.Business;
import com.example.demo.exception.business.BusinessNotCreatedException;
import com.example.demo.exception.business.BusinessNotFoundException;
import com.example.demo.service.BusinessService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;

@RestController
@RequestMapping("/business-account")
public class BusinessController {

    private final BusinessService businessService;

    public BusinessController(BusinessService businessService) {
        this.businessService = businessService;
    }

    @PostMapping()
    public ResponseEntity createBusiness(@RequestBody Business business) {
        try {
            businessService.createBusinessAccount(business);
            URI location = URI.create(String.format("/business-account/%s", business.getId()));
            return ResponseEntity.created(location).build();
        } catch (BusinessNotCreatedException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteBusiness(@PathVariable String id) {
        try {
            businessService.deleteBusinessAccount(id);
            return ResponseEntity.ok().build();
        } catch (BusinessNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity getBusinessAccountById(@PathVariable String id) {
        try {
            Business business = businessService.getBusinessAccountByID(id);
            return ResponseEntity.ok(business);
        } catch (BusinessNotFoundException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

}
