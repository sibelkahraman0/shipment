package com.example.demo.controller;

import com.example.demo.domain.BusinessShipment;
import com.example.demo.domain.Person;
import com.example.demo.domain.ShipmentPackage;
import com.example.demo.exception.businessShipment.BusinessShipmentNotCreatedException;
import com.example.demo.exception.businessShipment.BusinessShipmentNotFoundException;
import com.example.demo.service.BusinessShipmentService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/business")
public class BusinessShipmentController {

    private final BusinessShipmentService businessShipmentService;

    public BusinessShipmentController(BusinessShipmentService businessShipmentService) {
        this.businessShipmentService = businessShipmentService;
    }

    @GetMapping("/shipments/{trackingCode}")
    public ResponseEntity getShipmentByTrackingCode(@PathVariable String trackingCode) {
        try{
            return ResponseEntity.ok(businessShipmentService.getBusinessShipmentByTrackingCode(trackingCode));
        } catch (BusinessShipmentNotFoundException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/{id}/shipments")
    public ResponseEntity createShipmentByBusinessAccount(@PathVariable String id, @RequestBody BusinessShipment businessShipment) {
        try{
            businessShipmentService.createBusinessShipment(id, businessShipment);
            return ResponseEntity.ok().build();
        } catch (BusinessShipmentNotCreatedException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }

    @GetMapping("/{id}/shipments")
    public ResponseEntity getAllBusinessShipmentsByAccountId(@PathVariable String id) {
        try {
            return ResponseEntity.ok(businessShipmentService.getAllBusinessShipmentByAccountID(id));
        } catch (BusinessShipmentNotFoundException e) {
            e.printStackTrace();
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
