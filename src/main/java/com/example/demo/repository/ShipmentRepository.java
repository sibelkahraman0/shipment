package com.example.demo.repository;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.example.demo.domain.Address;
import com.example.demo.domain.Person;
import com.example.demo.domain.Shipment;
import com.example.demo.exception.shipment.ShipmentNotCreatedException;
import com.example.demo.exception.shipment.ShipmentNotFoundException;
import com.example.demo.exception.shipment.ShipmentNotStatusUpdatedException;
import com.example.demo.exception.shipment.ShipmentNotUpdatedException;
import org.springframework.stereotype.Repository;

@Repository
public class ShipmentRepository {

    private final Cluster couchbaseCluster;
    private final Collection shipmentCollection;

    public ShipmentRepository(Cluster couchbaseCluster, Collection shipmentCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.shipmentCollection = shipmentCollection;
    }

    public void insert(Shipment shipment) throws ShipmentNotCreatedException {
        if (!isShipmentValid(shipment))
            throw new ShipmentNotCreatedException("Shipment Cannot Created");
        shipmentCollection.insert(shipment.getTrackingCode(), shipment);
    }

    private boolean isShipmentValid(Shipment shipment) {
        return shipment.getPayer() != null && shipment.getShipmentType() != null && shipment.getStatus() != null &&
                isPersonValid(shipment.getSender()) && isPersonValid(shipment.getReceiver());

    }

    private boolean isPersonValid(Person person) {
        return person.getName() != null && person.getSurname() != null &&
                person.getPhone() != null && person.getAddress() != null;
    }

    public Shipment getByTrackingCode(String trackingCode) throws ShipmentNotFoundException {
        try {
            GetResult getResult = shipmentCollection.get(trackingCode);
            return getResult.contentAs(Shipment.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShipmentNotFoundException("Shipment Cannot Found");
        }
    }

    public void updateStatus(String trackingCode, String newStatus) throws ShipmentNotStatusUpdatedException {
        try {
            Shipment shipment = getByTrackingCode(trackingCode);
            shipment.setStatus(newStatus);
            shipmentCollection.replace(trackingCode, shipment);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShipmentNotStatusUpdatedException("Shipment Cannot Status Updated");
        }
    }

    public void updateShipment(String trackingCode, Address newAddress) throws ShipmentNotUpdatedException {
        try {
            Shipment shipment = getByTrackingCode(trackingCode);
            shipment.updateShipmentReceiverAddress(newAddress);
            shipmentCollection.replace(trackingCode, shipment);
        } catch (Exception e) {
            e.printStackTrace();
            throw new ShipmentNotUpdatedException("Shipment Cannot Updated");
        }
    }

}
