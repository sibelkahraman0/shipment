package com.example.demo.repository;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.example.demo.domain.Courier;
import com.example.demo.exception.courier.CourierNotCreatedException;
import com.example.demo.exception.courier.CourierNotFoundException;
import org.springframework.stereotype.Repository;

@Repository
public class CourierRepository {

    private final Cluster couchbaseCluster;
    private final Collection courierCollection;

    public CourierRepository(Cluster couchbaseCluster, Collection courierCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.courierCollection = courierCollection;
    }

    public void insert(Courier courier) throws CourierNotCreatedException {
        if(!isValidCourier(courier))
            throw new CourierNotCreatedException("Courier Not Created");
        courierCollection.insert(courier.getId(), courier);
    }

    private boolean isValidCourier(Courier courier) {
        return courier.getPerson() == null;
    }

    public String getDate(String id) throws CourierNotFoundException {
        try{
            GetResult getResult = courierCollection.get(id);
            return getResult.contentAs(Courier.class).getDate();
        }catch (Exception e){
            throw new CourierNotFoundException("Courier Not Found");
        }

    }

    public void update(String id, String date) throws CourierNotFoundException {
        try {
            GetResult getResult = courierCollection.get(id);
            Courier courier = getResult.contentAs(Courier.class);
            courier.setDate(date);
            courierCollection.replace(id,courier);
        } catch (Exception e){
            throw new CourierNotFoundException("Courier Not Found");
        }

    }
}
