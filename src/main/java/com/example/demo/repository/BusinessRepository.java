package com.example.demo.repository;

import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.example.demo.domain.Business;
import com.example.demo.exception.business.BusinessNotCreatedException;
import com.example.demo.exception.business.BusinessNotFoundException;
import org.springframework.stereotype.Repository;

@Repository
public class BusinessRepository {
    private final Cluster couchbaseCluster;
    private final Collection businessCollection;

    public BusinessRepository(Cluster couchbaseCluster, Collection businessCollection) {
        this.couchbaseCluster = couchbaseCluster;
        this.businessCollection = businessCollection;
    }

    public void insertBusinessAccount(Business business) throws BusinessNotCreatedException {
        if (!isBusinessValid(business)) {
            throw new BusinessNotCreatedException("Business account cannot created");
        }
        businessCollection.insert(business.getId(), business);
    }

    private boolean isBusinessValid(Business business) {
        return business.getAddress() != null && business.getEMail() != null && business.getName() != null && business.getPhone() != null;
    }

    public Business getBusinessAccountByID(String businessId) throws BusinessNotFoundException {
        try {
            return businessCollection.get(businessId).contentAs(Business.class);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessNotFoundException("Business Account Cannot Found");
        }
    }

    public void deleteBusinessAccountByID(String businessId) throws BusinessNotFoundException {
        try {
            businessCollection.remove(businessId);
        } catch (Exception e) {
            e.printStackTrace();
            throw new BusinessNotFoundException("Business Account Cannot Found");
        }
    }
}
