package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Person {
    private String name;
    private String surname;
    private String phone;
    private Address address;

    public Person() {
    }

    public Person(String name, String surname, String phone, Address address) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
        this.address = address;
    }

}
