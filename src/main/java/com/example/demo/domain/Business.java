package com.example.demo.domain;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter

public class Business {
    private String id;
    @NonNull
    private String name;
    @NonNull
    private String phone;
    @NonNull
    private String address;
    @NonNull
    private String eMail;

    public Business() {
        this.id = UUID.randomUUID().toString();
    }

    public Business(String name, String phone, String address, String eMail) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
        this.phone = phone;
        this.address = address;
        this.eMail = eMail;
    }
}
