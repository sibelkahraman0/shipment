package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class Shipment {
    private String id;
    private String trackingCode;
    private Person sender;
    private Person receiver;
    private ShipmentPackage shipmentPackage;
    private String shipmentType;
    private String payer;
    private String status;

    public Shipment() {
        this.trackingCode = UUID.randomUUID().toString();
        this.id = trackingCode;
    }

    public void updateShipmentReceiverAddress(Address newAddress) {
        this.receiver.setAddress(newAddress);
    }

    public int getPayment() {
        return (int)(shipmentPackage.getHeight()*shipmentPackage.getLength()*
                shipmentPackage.getWeight()*shipmentPackage.getWidth()/10);
    }
}
