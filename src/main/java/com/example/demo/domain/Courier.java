package com.example.demo.domain;

import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

@Getter
@Setter
public class Courier {

    private String id;
    private Person person;
    private String date;

    public Courier() {
        this.id = UUID.randomUUID().toString();
    }

    public void createAppointment(Person person, String date) {
        this.person = person;
        this.date = date;
    }

}
