package com.example.demo.service;

import com.example.demo.domain.Courier;
import com.example.demo.exception.courier.CourierNotCreatedException;
import com.example.demo.exception.courier.CourierNotFoundException;
import com.example.demo.repository.CourierRepository;
import org.springframework.stereotype.Service;

@Service
public class CourierService {
    private final CourierRepository courierRepository;

    public CourierService(CourierRepository courierRepository) {
        this.courierRepository = courierRepository;
    }

    public void createAppointment(Courier courier) throws CourierNotCreatedException {
        courierRepository.insert(courier);
    }

    public String getDate(String id) throws CourierNotFoundException {
       return courierRepository.getDate(id);
    }

    public void updateCourier(String id, String date) throws CourierNotFoundException {
        courierRepository.update(id,date);
    }
}
