package com.example.demo.service;

import com.example.demo.domain.BusinessShipment;
import com.example.demo.exception.business.BusinessNotCreatedException;
import com.example.demo.exception.businessShipment.BusinessShipmentNotCreatedException;
import com.example.demo.exception.businessShipment.BusinessShipmentNotFoundException;
import com.example.demo.repository.BusinessShipmentRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BusinessShipmentService {
    private final BusinessShipmentRepository businessShipmentRepository;

    public BusinessShipmentService(BusinessShipmentRepository businessShipmentRepository) {
        this.businessShipmentRepository = businessShipmentRepository;
    }
    public void createBusinessShipment(String id, BusinessShipment businessShipment) throws BusinessShipmentNotCreatedException{
             businessShipmentRepository.insertBusinessShipment(id, businessShipment);

    }
    public BusinessShipment getBusinessShipmentByTrackingCode(String trackingCode) throws BusinessShipmentNotFoundException {
        return businessShipmentRepository.getBusinessShipmentByTrackingCode(trackingCode);
    }
    public List<BusinessShipment> getAllBusinessShipmentByAccountID(String id) throws BusinessShipmentNotFoundException {
        return businessShipmentRepository.getAllBusinessShipmentByAccountID(id);
    }
}
