package com.example.demo.service;

import com.example.demo.domain.Address;
import com.example.demo.domain.Shipment;
import com.example.demo.domain.ShipmentPackage;
import com.example.demo.exception.shipment.*;
import com.example.demo.repository.ShipmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ShipmentService {
    private final ShipmentRepository shipmentRepository;

    @Autowired
    public ShipmentService(ShipmentRepository shipmentRepository) {
        this.shipmentRepository = shipmentRepository;
    }

    public void createShipment(Shipment shipment) throws ShipmentNotCreatedException {
        shipmentRepository.insert(shipment);
    }

    public Shipment getShipmentByTrackingCode(String trackingCode) throws ShipmentNotFoundException {
        return shipmentRepository.getByTrackingCode(trackingCode);
    }

    public void updateStatus(String trackingCode, String status) throws ShipmentNotStatusUpdatedException {
        shipmentRepository.updateStatus(trackingCode, status);
    }

    public void updateShipment(String trackingCode, Address address) throws ShipmentNotUpdatedException {
        shipmentRepository.updateShipment(trackingCode, address);
    }

    public int calculatePayment(int width, int height, int weight, int length) throws ShipmentNotCalculatedException {
        if (width < 0 && height < 0 && weight < 0 && length < 0)
            throw new ShipmentNotCalculatedException("Package size cannot be smaller than 0");

        return (int) (width * height * weight * length / 10);
    }
}
