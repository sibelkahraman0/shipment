package com.example.demo.exception.businessShipment;

public class BusinessShipmentNotFoundException extends Exception{
    public BusinessShipmentNotFoundException(String message){
        super(message);
    }
}
