package com.example.demo.exception.businessShipment;

public class BusinessShipmentNotCreatedException extends Exception {
    public BusinessShipmentNotCreatedException(String message) {
        super(message);
    }
}
