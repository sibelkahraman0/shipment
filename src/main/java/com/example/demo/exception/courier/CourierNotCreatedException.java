package com.example.demo.exception.courier;

public class CourierNotCreatedException extends Exception{
    public CourierNotCreatedException (String message){
        super(message);
    }
}
