package com.example.demo.exception.courier;

public class CourierNotFoundException extends Exception{
    public CourierNotFoundException(String message){
        super(message);
    }
}
