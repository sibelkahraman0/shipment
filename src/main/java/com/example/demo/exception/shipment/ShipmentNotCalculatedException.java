package com.example.demo.exception.shipment;

public class ShipmentNotCalculatedException extends Exception {
    public ShipmentNotCalculatedException(String message) {
        super(message);
    }
}
