package com.example.demo.exception.shipment;

public class ShipmentNotStatusUpdatedException extends Exception{
    public ShipmentNotStatusUpdatedException(String message){
        super(message);
    }
}
