package com.example.demo.exception.shipment;

public class ShipmentNotCreatedException extends Exception {
    public ShipmentNotCreatedException(String message) {
        super(message);
    }
}
