package com.example.demo.exception.shipment;

public class ShipmentNotUpdatedException extends Exception{
    public ShipmentNotUpdatedException(String message){
        super(message);
    }
}