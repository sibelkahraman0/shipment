package com.example.demo.exception.shipment;

public class ShipmentNotFoundException extends Exception {
    public ShipmentNotFoundException(String message){
        super(message);
    }
}
