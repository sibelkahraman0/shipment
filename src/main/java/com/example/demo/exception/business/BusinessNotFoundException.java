package com.example.demo.exception.business;

public class BusinessNotFoundException extends Exception{
    public BusinessNotFoundException(String message){
        super(message);
    }
}
