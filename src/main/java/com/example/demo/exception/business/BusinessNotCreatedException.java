package com.example.demo.exception.business;

public class BusinessNotCreatedException extends Exception{
    public BusinessNotCreatedException(String message){
        super(message);
    }
}
